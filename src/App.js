import Login from './views/Login';
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import './styling/App.css';
import Translate from './views/Translate';
import Profile from './views/Profile';
import { UserContext } from './UserContext';
import React, { useState } from 'react'

function App() {
  const [userName, setUserName] = useState("")
  const [loggedIn, setLoggedIn] = useState(false)
  const [id, setId] = useState("")
  const [searches, setSearches] = useState("")
  return (
    <BrowserRouter>
      <div className="App">
        <UserContext.Provider value={{userName, setUserName, loggedIn, 
          setLoggedIn, id, setId, searches, setSearches}}>
        <Routes>
            <Route path='/' element={<Login/>}/>
            <Route path='/translate' element={<Translate/>}/>
            <Route path='/profile' element={<Profile/>}/>
        </Routes>
        </UserContext.Provider>
      </div>
    </BrowserRouter>
    
  );
}

export default App;
