import React from 'react'
import Header from '../components/Header'
import SearchHistory from '../components/profile/SearchHistory'

function Profile() {
  return (
    <div>
      <Header/>
      <h2>Recent Searches</h2>
      <SearchHistory/>
    </div>
  )
}

export default Profile