import React, { useContext, useState,} from "react";
import Header from "../components/Header";
import TranslationArea from "../components/translation/TranslationArea";
import TranslationForm from "../components/translation/TranslationForm";
import { UserContext } from "../UserContext";
import Container from "@mui/material/Container";

function Translate() {
	const { userName, id, searches, setSearches } = useContext(UserContext);

	const [wordObject, setWordObject] = useState({
		word: "",
		letters: [],
	});

	const handleInputChange = (event) => {
		setWordObject({
			...wordObject,
			word: event.target.value,
		});
	};

	const handleSubmit = (event) => {
		event.preventDefault();

		setWordObject({
			...wordObject,
			letters: wordObject.word.toLowerCase().split(""),
		});

		/*
        If words lenght is higher than 40, 
        alert the user and reset word value back to default
    	*/
		if (wordObject.word.length > 40) {
			alert("Word is too long");
			setWordObject({
				...wordObject,
				word: "",
			});
		} else if (wordObject.word.match(/^([a-zA-Z]+\s)*[a-zA-Z]+$/)) {

		/*
      	Regex checks that the word contains only letters
      	and max one space between them. If true, save translated text to api
      	*/
			// Save translation to API
			const newArr = [...searches];
			newArr.push(wordObject.word);
			setSearches(newArr);
			addToApi(newArr);
		} else {

		/*
        If everything else fails, alert user with "Invalid input" 
        and set word back to default value
      	*/
			alert("Invalid input");
			setWordObject({
				...wordObject,
				word: "",
			});
		}
	};

	const addToApi = (array) => {
		fetch("https://velo-noroff-api.herokuapp.com/translations/" + id, {
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				// eslint-disable-next-line no-undef
				"x-api-key": process.env.REACT_APP_API_KEY,
			},
			method: "PATCH",
			body: JSON.stringify({
				translations: array,
			}),
		});
	};

	return (
		<>
			<Header />
			<h3>
				Translation page
			</h3>

			<TranslationForm
				word={wordObject.word}
				handleSubmit={handleSubmit}
				handleInputChange={handleInputChange}
			/>

			<Container sx={{
				backgroundColor: "#D9F8C4",
				width: '50%',
				height: 230,
				borderRadius: 2,
			}}>
				{wordObject.letters.length > 0 && (
					<div>
						<h3>Translation:</h3>
						<TranslationArea letters={wordObject.letters} />
					</div>
				)}
			</Container>
		</>
	);
}

export default Translate;
