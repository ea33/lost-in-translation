import React from 'react'
import LoginForm from '../components/login/LoginForm'

function Login() {
  return (
    <div>
      <h3>Login</h3>
      <LoginForm/>
    </div>
    
  )
}

export default Login