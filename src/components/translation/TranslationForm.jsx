import Button from "@mui/material/Button";
import Input from "@mui/material/Input";

const TranslationForm = (props) => {
	return (
		<form onSubmit={props.handleSubmit}>
			<label htmlFor="word">Enter the text you want to translate</label>
			<div>
				<Input
					sx={{
						mt: 5,
						width: 400,
					}}
					variant="outlined"
					id="word"
					type="text"
					value={props.word}
					placeholder="Enter your text"
					onChange={props.handleInputChange}
				/>
				<Button
					type="submit"
					variant="contained"
					sx={{
						mt: 5,
						mb : 5,
						color: "black",
						backgroundColor: "#FAD9A1",
						borderRadius: 3,
						left: 50,
						"&:hover": {
							backgroundColor: "#F9F9C5",
						},
					}}
				>
					Translate
				</Button>
			</div>
		</form>
	);
};
export default TranslationForm;
