import TranslationImage from "./TranslationImage";

const TranslationArea = (props) => {
	return (
		<div>
			{props.letters.map((letter, index) => {
				if (letter.match(/\w/)) {
					return (
						<TranslationImage
						src={`./individial_signs/${letter}.png`}
						key={index}
						/>
						);
					}
					if (letter === " ") {
					return <p key={{index}} />;
				}
				return <textarea hidden key={index} />;
			})}
		</div>
	);
};
export default TranslationArea;
