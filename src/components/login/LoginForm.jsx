import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { UserContext } from "../../UserContext";
import Input from "@mui/material/Input";
import Button from "@mui/material/Button";
import Container from "@mui/material/Container";

function LoginForm() {
	const { setUserName, userName, setLoggedIn, setId, setSearches } =
		useContext(UserContext);
	let navigate = useNavigate();

	const routeChange = () => {
		let path = "/translate";
		navigate(path);
	};

	const onSubmit = (e) => {
		e.preventDefault();
		routeChange();
		findUser();
		setLoggedIn(true);
	};

	const findUser = () => {
		fetch("https://velo-noroff-api.herokuapp.com/translations")
			.then((res) => res.json())
			.then((json) => json.find((e) => e.username === userName))
			.then((data) => createUser(data));
	};

	const createUser = (data) => {
		if (data !== undefined) {
			saveUserDataToContext();
			return;
		}
		fetch("https://velo-noroff-api.herokuapp.com/translations", {
			method: "POST",
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json",
				"x-api-key": process.env.REACT_APP_API_KEY,
			},
			body: JSON.stringify({
				username: userName,
				translations: [],
			}),
		}).then(() => saveUserDataToContext());
	};
	const saveUserDataToContext = () => {
		fetch("https://velo-noroff-api.herokuapp.com/translations")
			.then((res) => res.json())
			.then((json) => json.find((e) => e.username === userName))
			.then((data) => {
				setId(data.id);
				setSearches(data.translations);
			});
	};

	return (
		<div>
			<form onSubmit={onSubmit}>
				<label>
					<div>
						<Container sx={{
							backgroundColor: "#D9F8C4",
							borderRadius: 1,
							border: 1,
							width: 500,
						}}>
							<Input
								sx={{
									width: 200,
									left: 80,
								}}
								type="text"
								placeholder="username"
								onChange={(e) => setUserName(e.target.value)}
							/>
							<Button
								sx={{
									backgroundColor: "#FAD9A1",
									borderRadius: 1,
									left: 40,
									padding: 2,
									margin: 10,
									border: 1,
								}}
								type="submit"
							>
								Login
							</Button>
						</Container>
					</div>
				</label>
			</form>
		</div>
	);
}

export default LoginForm;
