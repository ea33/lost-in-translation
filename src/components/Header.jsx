import { Button } from "@mui/material";
import React, { useContext, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { UserContext } from "../UserContext";
import "../styling/Header.css";
import AccountBoxIcon from '@mui/icons-material/AccountBox';
import KeyboardIcon from '@mui/icons-material/Keyboard';
import Tooltip from '@mui/material/Tooltip';
import LogoutIcon from '@mui/icons-material/Logout';

function Header() {
	const {setUserName, loggedIn, setLoggedIn, userName } =
		useContext(UserContext);
	let navigate = useNavigate();

	const routeChange = () => {
		let path = "/";
		navigate(path);
	};

	const checkAccess = () => {
		if (!loggedIn) {
			routeChange();
		}
	};

	useEffect(() => {
		checkAccess();
	}, []);

	const logout = () => {
		setUserName("");
		setLoggedIn(false);
		routeChange();
	};

	return (
		<div className="header">
			<nav className="nav-bar">
				<ul className="nav-table">
					<li className="headerLink">
						<Tooltip title="Translate">
							<Link to="/translate"><KeyboardIcon color="primary" fontSize="large"/></Link>
						</Tooltip>
					</li>
					<li className="headerLink">
						<Tooltip title="Profile">
							<Link to="/profile"><AccountBoxIcon color="primary" fontSize="large"/></Link>
						</Tooltip>
					</li>
				</ul>
				<div className="username">
					<h3>{userName}</h3>
				</div>
				<div className="logout">
					<Button variant="contained" startIcon={<LogoutIcon/>} size="small" onClick={logout}>Logout</Button>
				</div>
			</nav>
		</div>
	);
}

export default Header;
