import React, { useState,useContext } from 'react'
import { UserContext } from '../../UserContext'
import Button from '@mui/material/Button'
import "../../styling/Profile.css"
import DeleteIcon from '@mui/icons-material/Delete'
import signLanguage from "../../signLanguage.gif"
import { borderRadius } from '@mui/system'

function SearchHistory() {

    const {userName, id, setSearches} = useContext(UserContext);
    const [recentSearches, setRecentSearches] = useState([])
    const [loaded, setLoaded] = useState(false)

    fetch("https://velo-noroff-api.herokuapp.com/translations")
    .then(res => res.json())
    .then(json => json.find(e => e.username === userName))
    .then( data => {
      let startingIndex = 0
      if (data.translations.length > 10){
        startingIndex = data.translations.length - 10
      }
      setRecentSearches(data.translations.slice(startingIndex, startingIndex+10))
    })
    .then(() => setLoaded(true))

    const clearSearches = () => {
        setSearches([])
        fetch("https://velo-noroff-api.herokuapp.com/translations/"+id, {
			headers: {
				Accept : "application/json",
				"Content-Type" : "application/json",
				"x-api-key" : process.env.REACT_APP_API_KEY
			},
			method : "PATCH",
			body : JSON.stringify({
				translations : []
			})
		})
    }



  return (
    <div className='parentContainer'>
        <div className='historyContainer'>
          <img src={signLanguage} style={{width:"250px", height:"200px", margin:"80px",
          borderRadius:"10px"}} alt="GIF" />
          <div className='searchList'>
            { loaded ? recentSearches.map((element, i) =><li className='listElement' key={i}>{element}</li>)
            : <div>Loading recent searches...</div> }
          </div>
          <img src={signLanguage} style={{width:"250px", height:"200px", margin:"80px",
          borderRadius:"10px"}} alt="GIF" />
        </div>
        <div>
        <Button variant="contained" startIcon={<DeleteIcon/>} onClick={clearSearches}>Clear Searches</Button>
        </div>
    </div>
  )
}

export default SearchHistory