# Lost-In-Translation

This web application is developed using React.js to translate english to American sign language. App has a Login page where user must input their username to access the page. In translation page user can type up to 40 characters that will then be translated to sign language. Profile page will show users 10 latest translations.

## Installation
```
$ git clone git@gitlab.com:ea33/lost-in-translation.git
$ cd lost-in-translation
$ npm install
$ npm start
```

## Contributors

Thien Nguyen <br>
Valtteri Elo